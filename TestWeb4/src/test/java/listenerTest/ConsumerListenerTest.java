package listenerTest;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import listener.ConsumerListener;

public class ConsumerListenerTest {
	private static Logger logger = LogManager.getLogger(ConsumerListenerTest.class.getName());
	private TextMessage message;
	private ApplicationContext context;
	private ConsumerListener listener;
	private String json = "{\r\n" + 
			"  vendorName: \"Mongotest\",\r\n" + 
			"  firstName: \"IliTest\",\r\n" + 
			"  lastName: \"BenTest\",\r\n" + 
			"  address: \"456 Main test\",\r\n" + 
			"  city: \"Test\",\r\n" + 
			"  state: \"OKTest\",\r\n" + 
			"  zip: \"33Test\",\r\n" + 
			"  email: \"Ili@microsoft.test\",\r\n" + 
			"  phoneNumber: \"test-456-test\"\r\n" + 
			"}";


	@Before
	public void setUp() throws ClassNotFoundException {

		context = new ClassPathXmlApplicationContext("/spring/application-config.xml");
		listener = (ConsumerListener) context.getBean("consumerListener");
		message = createMock(TextMessage.class);
	}

	@After
	public void tearDown() throws Exception {
		((ConfigurableApplicationContext)context).close();
	}

	@Test
	public void testOnMessage() throws JMSException{
		expect(message.getText()).andReturn(json);
		logger.info(json);
		replay(message);
		listener.onMessage(message);
		verify(message);
	}

}