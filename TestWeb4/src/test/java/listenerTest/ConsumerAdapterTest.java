package listenerTest;

import static org.junit.Assert.*;

import java.net.UnknownHostException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import adapter.ConsumerAdapter;

public class ConsumerAdapterTest {

	private String json = "{\r\n" + 
			"  vendorName: \"Microsofttest\",\r\n" + 
			"  firstName: \"IlTest\",\r\n" + 
			"  lastName: \"SmithTest\",\r\n" + 
			"  address: \"123 Main test\",\r\n" + 
			"  city: \"TulsaTest\",\r\n" + 
			"  state: \"OKTest\",\r\n" + 
			"  zip: \"71345Test\",\r\n" + 
			"  email: \"Bob@microsoft.test\",\r\n" + 
			"  phoneNumber: \"test-123-test\"\r\n" + 
			"}";
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSendToMongo() throws UnknownHostException{
		ConsumerAdapter adapter = new ConsumerAdapter();
		adapter.sendToMongo(json);
		assertNotNull(json);
	}

}
